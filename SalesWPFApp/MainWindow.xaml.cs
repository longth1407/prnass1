﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessObject;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Member member;
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Member member)
        {
            InitializeComponent();
        }

        private void sOut_Click(object sender, RoutedEventArgs e)
        {
            WindowLogin l = new WindowLogin();
            l.Show();
            this.Close();            
        }

        private void proM_Click(object sender, RoutedEventArgs e)
        {
            new WindowProducts(member).Show();            
        }

        private void ordM_Click(object sender, RoutedEventArgs e)
        {
            new WindowOrders(member).Show();            
        }

        private void memM_Click(object sender, RoutedEventArgs e)
        {
            new WindowMembers(member).Show();            
        }

        private void sExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
