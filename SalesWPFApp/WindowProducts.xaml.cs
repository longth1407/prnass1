﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessObject;
using Microsoft.IdentityModel.Tokens;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for WindowProducts.xaml
    /// </summary>
    public partial class WindowProducts : Window
    {
        FstoreContext context = new FstoreContext();
       
        public WindowProducts()
        {
            InitializeComponent();
        }

        public WindowProducts(Member member)
        {
            InitializeComponent();
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            myPopup.IsOpen = true;
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void int_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {
            if (validateData())
            {
                Product pro = new Product
                {
                    ProductName = pName.Text.Trim(),
                    CategoryId = int.Parse(cID.Text.Trim()),
                    Weight = pWeight.Text.Trim(),
                    UnitPrice = decimal.Parse(pPrice.Text.Trim()),
                    UnitslnStock = int.Parse(pUIS.Text.Trim())
                };
                context.Products.Add(pro);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    myPopup.IsOpen = false;
                    MessageBox.Show("Product Successfully Added!");                    
                }
                else
                {
                    myPopup.IsOpen = false;
                    MessageBox.Show("Product Added Failed!");
                }
            }
            loadProduct();
        }

        private bool validateData()
        {
            bool isValid = true;
            string message = "";
            if (string.IsNullOrEmpty(pName.Text.Trim()))
            {
                isValid = false;
                message += "Name cannot be empty!\n";
            }
            if (string.IsNullOrEmpty(cID.Text.Trim()))
            {
                isValid = false;
                message += "Category ID cannot be empty!\n";
            }
            if (string.IsNullOrEmpty(pWeight.Text.Trim()))
            {
                isValid = false;
                message += "Weight cannot be empty!\n";
            }
            if (string.IsNullOrEmpty(pPrice.Text.Trim()))
            {
                isValid = false;
                message += "Unit Price cannot be empty!\n";
            }
            if (string.IsNullOrEmpty(pUIS.Text.Trim()))
            {
                isValid = false;
                message += "Units ln Stock cannot be empty!\n";
            }
            if (!isValid)
            {
                //MessageBox.Show("[Error] Data Invalid: \n" + message);
                noti.Content = message;
            }
            return isValid;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            myPopup.IsOpen = false;
        }

        private void dec_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
            e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadProduct();
        }

        private void loadProduct()
        {
            var p = (from pr in context.Products
                     select new
                     {
                         pr.ProductId,
                         pr.CategoryId,
                         pr.ProductName,
                         pr.Weight,
                         pr.UnitPrice,
                         pr.UnitslnStock
                     }).ToList();
            lvProducts.ItemsSource = p;
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            var result = searchText.Text;
            if (result == null || result == "")
            {
                loadProduct();
            }
            else
            {
                var p = (from pr in context.Products
                         where pr.ProductName.Contains(result)
                         select new
                         {
                             pr.ProductId,
                             pr.CategoryId,
                             pr.ProductName,
                             pr.Weight,
                             pr.UnitPrice,
                             pr.UnitslnStock
                         }).ToList();
                lvProducts.ItemsSource = p;
            }
        }

        private void reportButton_Click(object sender, RoutedEventArgs e)
        {
            myReport.IsOpen = true;
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /*var selectedItem = ((ListBoxItem)ListView.ContainerFromElement(Button)sender);
                lvProducts.SelectedItem = (ListBoxItem)selectedItem;
                var pro = context.Products.SingleOrDefault(p => p.ProductId == );
                if(pro != null)
                {
                    context.Products.Remove(pro);
                    int count = context.SaveChanges();
                    if(count > 0)
                    {
                        MessageBox.Show("Delete Succesfully!");
                        loadProduct();
                    }
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show("Delete Error: " + ex.Message);
            }
        }

        private void cancelR_Click(object sender, RoutedEventArgs e)
        {
            myReport.IsOpen = false;
            loadProduct();
        }

        private void submitR_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
