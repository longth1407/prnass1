﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessObject;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for WindowMember.xaml
    /// </summary>
    public partial class WindowMembers : Window
    {
        FstoreContext context = new FstoreContext();
        public WindowMembers()
        {
            InitializeComponent();
        }

        public WindowMembers(Member member)
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            myPopup.IsOpen = false;
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            myPopup.IsOpen = true;
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadMember();
        }

        private void loadMember()
        {
            var m = (from i in context.Members
                     select new
                     {
                         i.MemberId,
                         i.Email,
                         i.CompanyName,
                         i.City,
                         i.Country,
                         i.Password
                     }).ToList();
            lvMembers.ItemsSource = m;
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
